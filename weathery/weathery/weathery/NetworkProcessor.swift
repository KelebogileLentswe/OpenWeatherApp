//
//  NetworkProcessor.swift
//  weathery
//
//  Created by Thato Lentswe on 05/12/17.
//  Copyright © 2017 Thato Lentswe. All rights reserved.
//

import Foundation

class NetworkProcessor {
    lazy var config : URLSessionConfiguration = URLSessionConfiguration.default
    lazy var urlsession : URLSession = URLSession(configuration: self.config )
    let url : URL
    init(url: URL) {
     self.url = url
    }
    // could use alarmofire or SwiftyJson for getting data from the api to the app
  typealias JSONHandler = (([String: AnyObject ]?) -> Void)
    
    func downloadJsonFromHandler(_ completion: @escaping JSONHandler) {
        
        //request to download data 
        let request = URLRequest(url: self.url)
        //put the downloaded data into a local variable
        let dataTask = urlsession.dataTask(with: request) { (data, response, error) in
            
            //checking the error exists
            if error == nil {
                
                // check the response from api
                if let httpResponse = response as? HTTPURLResponse {
                    
                    switch (httpResponse.statusCode) {
                        
                        //successful response
                    case 200:
                        //parsing the data to json dictionary
                        if let data = data {
                            do
                            {
                                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                                
                                //passing data to completion handler and cast it
                                completion(jsonDictionary as? Dictionary <String, AnyObject>)
                                
                            }
                            catch let error as NSError {
                                
                                print("Èror processing json data: \(error.localizedDescription)")
                            }
                            
 
                        }
                        
                        
                    default:
                        print("HTTP Response Code: \(httpResponse.statusCode)")
                    }
                }
                
            } else {
                print("Error: \(error?.localizedDescription)")
            }
        }
        dataTask.resume()
    }
}
