//
//  ForecastService.swift
//  weathery
//
//  Created by Thato Lentswe on 06/12/17.
//  Copyright © 2017 Thato Lentswe. All rights reserved.
//

import Foundation

class  ForecastService {
    
    let forecastApi : String
    
    let forecastBaseUrl : URL?
    
    //forecastbase url/ forecastAPi/
    init(Apikey : String)
    {
        self.forecastApi = Apikey
        
//       http://api.openweathermap.org/data/2.5/weather?APPID=06db44f389d2172e9b1096cdce7b051c&lat=35&lon=139
        forecastBaseUrl = URL(string: "https://api.openweathermap.org/data/2.5/weather?APPID=\(Apikey)&")
    }
   
    func getForecast(lat: Double, long: Double, completion: @escaping(Weather?) -> Void) {
       
        //if let forecastUrl = URL(String: "lat=\(lat)&lon=\(long)", relativeTo: forecastURL!)
        if let forecastURL = URL(string: "\(forecastBaseUrl!)lat=\(lat)&lon=\(long)") {
            
            let networkProcessor = NetworkProcessor(url: forecastURL)
            networkProcessor.downloadJsonFromHandler({(jsonDictionary) in
                
                // If we made it to this point, we've successfully converted the
                // JSON-formatted weather data into a Swift dictionary.
                // Let's now used that dictionary to initialize a Weather struct.
                
                
                let currentWeather = Weather(weatherData: (jsonDictionary)!)
                    
                completion(currentWeather)
 
     })
        }
        
    }
   }

