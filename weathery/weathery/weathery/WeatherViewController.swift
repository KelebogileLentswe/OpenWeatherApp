//
//  WeatherViewController.swift
//  weathery
//
//  Created by Thato Lentswe on 06/12/17.
//  Copyright © 2017 Thato Lentswe. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController, CLLocationManagerDelegate {
    
    //@IBOutlet weak var UIDateTimeLabel: UILabel!
    @IBOutlet weak var UIDateTimeLabel: UILabel!
    
    //@IBOutlet weak var UICityLabel: UILabel!

    @IBOutlet weak var UICityLabel: UILabel!
    
    @IBOutlet weak var UIIconLabel: UIImageView!
    

    @IBOutlet weak var errorLabel: UILabel!
    
    
   @IBOutlet weak var UITempMaxLabel: UILabel!
  
    
  
    @IBOutlet weak var UITempMinLabel: UILabel!
    

 
    
    let forecastApiKey = "06db44f389d2172e9b1096cdce7b051c"
    
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation: AnyObject = locations[locations.count - 1]
        let latitude = latestLocation.coordinate.latitude
        let longitude = latestLocation.coordinate.longitude
        
        
        
     let forecastService = ForecastService(Apikey: forecastApiKey)
        forecastService.getForecast(lat: latitude, long: longitude) { (Weather) in
//        forecastService.getForecast(lat: lat, long: lon) { (CurrentWeather) in
        
            if let currentWeather = Weather {
                

                DispatchQueue.main.async {
                     self.UITempMaxLabel.text = "Max: \(Int(round(currentWeather.tempMaxCelsius)))°C"

                    self.UITempMinLabel.text="Min: \(Int(round(currentWeather.tempMinCelsius)))°C"

                if let dateTime = currentWeather.dateTime {
                    
                var mydate = Date()//my actual date need to display
                let dateFormatter = DateFormatter()
                if Calendar.current.isDateInToday(dateTime as Date) {
                    dateFormatter.dateFormat = "'Today,' d MMMM YYYY"
                }
                else if Calendar.current.isDateInYesterday(dateTime as Date) {
                    dateFormatter.dateFormat = "'Yesterday,' d MMMM YYYY"
                    
                }
                else if Calendar.current.isDateInTomorrow(dateTime as Date) {
                    dateFormatter.dateFormat = "'Tomorrow,' d MMMM YYYY"
                }
                else{
                    dateFormatter.dateFormat = "EEEE, d MMMM"
                }
                   //print(dateFormatter.string(from: mydate))
            self.UIDateTimeLabel.text = "\(dateFormatter.string(from: mydate))"
                    
                }
                else{
                    self.UIDateTimeLabel.text = "-"
                }
                if let icon = currentWeather.icon{
                    
                    self.UIIconLabel.image = UIImage (named: currentWeather.icon!)
                } else {
                    
                    self.errorLabel.text = "Error loading the image..."
                }

                    var code = currentWeather.country!
                    //for code in NSLocale.isoCountryCodes as [String] {
                        let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
                        let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
                   
               self.UICityLabel.text = "\(currentWeather.city!),\(name)"
            
                }
        }
        }
    func locationManager(_ manager: CLLocationManager,
    didFailWithError error: Error) {
    print("Error: " + error.localizedDescription)
    }
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        startLocation = nil
        
    }


}
