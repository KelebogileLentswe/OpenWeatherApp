//
//  currentWeather.swift
//  weathery
//
//  Created by Thato Lentswe on 06/12/17.
//  Copyright © 2017 Thato Lentswe. All rights reserved.
//
//This is a weather object
import Foundation
class Weather {
    
    let dateTime: NSDate?
    let icon: String?
    let country: String?
    let city: String?
    //let name: String?
    let weatherDescription: String?
    private let temp_min: Double
            var tempMinCelsius: Double {
                get {
                    return temp_min - 273.15
                }
            }
    private let temp_max: Double
    var tempMaxCelsius: Double {
        get {
            return temp_max - 273.15
        }
    }
    init(weatherData: [String: AnyObject]) {
                    dateTime = NSDate(timeIntervalSince1970: weatherData["dt"] as! TimeInterval)
       
                    //name = weatherData["name"] as? String
                     city = weatherData["name"] as? String
                    let weatherDict = weatherData["weather"]![0] as! [String: AnyObject]
                    weatherDescription = weatherDict["description"] as? String
                   icon = weatherDict["icon"] as? String
        
                    let mainDict = weatherData["main"] as! [String: AnyObject]
                    temp_max = mainDict["temp_max"] as! Double
                    temp_min = mainDict["temp_min"] as! Double

                    let sysDict = weatherData["sys"] as! [String: AnyObject]
                    country = sysDict["country"] as? String
                }
}

